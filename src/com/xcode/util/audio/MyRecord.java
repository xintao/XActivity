package com.xcode.util.audio;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;


public class MyRecord extends AudioRecord
{
	int bufferSize = 0;
	byte[] buffer = null;
	/**
	 * 
	 * @param sampleRateInHz	频率  推荐 44100Hz 其他的 22050, 16000, and 11025 有些设备可能不支持
	 * @param channelConfig		推荐 AudioFormat.CHANNEL_IN_MONO 其他的 AudioFormat.CHANNEL_IN_STEREO 有些设备可能不支持
	 * @param audioFormat		AudioFormat.ENCODING_PCM_16BIT 音质高  AudioFormat.ENCODING_PCM_8BIT 音质低功耗低
	 */
	public MyRecord(int sampleRateInHz, int channelConfig,int audioFormat)
	{
		super(MediaRecorder.AudioSource.MIC, sampleRateInHz, channelConfig, audioFormat,AudioRecord.getMinBufferSize(sampleRateInHz,channelConfig,audioFormat));
		bufferSize = AudioRecord.getMinBufferSize(sampleRateInHz,channelConfig,audioFormat);
		buffer = new byte[bufferSize];
	}
	
	/**
	 * @return 获得一个推荐的配置的录音对象
	 */
	public static MyRecord getDefaultRecord()
	{
		return new MyRecord(44100,AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
	}
	
	/**
	 * 开始录制
	 */
	public void start()
	{
		this.startRecording();// 开始录制
	}
	
	/**
	 * @return	获取录下来的声音片段
	 */
	public byte[] getReadByte()
	{
		int readLen = this.read(buffer, 0,bufferSize);
		byte[] tmpBuf = new byte[readLen];
		System.arraycopy(buffer, 0, tmpBuf, 0, readLen);
		return tmpBuf;
	}
	
	/**
	 * 释放资源
	 */
	public void destory()
	{
		this.release();
	}
}
