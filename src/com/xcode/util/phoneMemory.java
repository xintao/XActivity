package com.xcode.util;

import java.io.File;
import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.text.format.Formatter;

/**
 * 对手机内部存储 或者内存卡一个简单的工具封装
 * @author 肖蕾
 *
 */
public class phoneMemory
{
	/**
	 * SD卡是否有用
	 * @return true 有用 false 没有
	 */
	public static boolean isSDCardUseful()
	{
		if (Environment.MEDIA_MOUNTED.equals(Environment .getExternalStorageState()))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * 获取SD卡的File对象
	 * @return SD卡的File对象
	 */
	public static File getSDCardDirectory()
	{
		return Environment.getExternalStorageDirectory();
	}
	
	/**
	 * 获取手机内部存储的File对象
	 * 
	 * @return 手机内部存储的File对象
	 */
	public static File getPhoneDataDirectory()
	{
		return Environment.getDataDirectory();
	}
	
	/**
	 * 获取SD卡的总大小 为什么不直接返回格式化的<br>
	 * 因为可能开发中需要内存的做比较
	 * @return SD卡的总大小
	 */
	@SuppressWarnings("deprecation")
	public static long getSDCardSize()
	{
		File file = getSDCardDirectory();
		StatFs statFs = new StatFs(file.getPath());
		long blockSize = statFs.getBlockSize();
		long blockCount = statFs.getBlockCount();
		return blockCount * blockSize;
	}
	
	/**
	 * 获取SD卡的可用大小 为什么不直接返回格式化<br> 
	 * 因为可能开发中需要内存的做比较
	 * @return SD卡的可用大小
	 */
	@SuppressWarnings("deprecation")
	public static long getSDcardFreeSize()
	{
		File file = getSDCardDirectory();
		StatFs statFs = new StatFs(file.getPath());
		long blockSize = statFs.getBlockSize();
		long availableBlocks = statFs.getAvailableBlocks();
		return availableBlocks * blockSize;
	}
	/**
	 * 获取手机总内存空间 为什么不直接返回格式化<br> 
	 * 因为可能开发中需要内存的做比较
	 * @return 手机总的内存空间
	 */
	@SuppressWarnings("deprecation")
	public static long getPhoneSize()
	{
		File file = getPhoneDataDirectory();
		StatFs statFs = new StatFs(file.getPath());
		long blockSize = statFs.getBlockSize();
		long blockCount = statFs.getBlockCount();
		return blockCount * blockSize;
	}
	
	/**
	 * 获取手机可用内存空间 为什么不直接返回格式化<br> 
	 * 因为可能开发中需要内存的做比较
	 * @return 手机可用内存空间
	 */
	@SuppressWarnings("deprecation")
	public static long getPhoneFreeSize()
	{
		File file = getPhoneDataDirectory();
		StatFs statFs = new StatFs(file.getPath());
		long blockSize = statFs.getBlockSize();
		long availableBlocks = statFs.getAvailableBlocks();
		return availableBlocks * blockSize;
	}
	
	/**
	 * 将数字形式的内存格式化成易于理解的格式：1M，2G，3K
	 * @param context 		上下文
	 * @param memorySize 	内存大小
	 * @return 				格式化之后的字符串
	 */
	public static String formatMemorySize(Context context,long memorySize)
	{
		return Formatter.formatFileSize(context, memorySize);
	}
}
