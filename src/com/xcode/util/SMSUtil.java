package com.xcode.util;

import java.util.ArrayList;
import java.util.List;

import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.SmsManager;

import com.xcode.bean.SMS;

/**
 * 对短信的操作进行了一系列的封装
 * @author 肖蕾
 *
 */
public class SMSUtil
{
	public static final String SMS_Uri = "content://sms";
	/**
	 * 获取所有短信
	 * @param context 上下文
	 * @since 	对于短信的类型Type 接收到的是1 发出去的是2
	 * @return 所有短信的SMS的list集合
	 */
	public static List<SMS> getAllSMS(Context context)// 获取所有短信
	{
		List<SMS> list = new ArrayList<SMS>();
		Uri uri = Uri.parse(SMS_Uri);
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(uri, new String[] { "address", "read", "date", "type", "body" }, null, null, null);
		while (cursor.moveToNext())
		{
			String address = cursor.getString(cursor.getColumnIndex("address"));
			long date = cursor.getLong(cursor.getColumnIndex("date"));
			int type = cursor.getInt(cursor.getColumnIndex("type"));
			String body = cursor.getString(cursor.getColumnIndex("body"));
			int hasread = cursor.getInt(cursor.getColumnIndex("read"));
			list.add(new SMS(address, date, type, body, hasread));
		}
		cursor.close();
		return list;
	}
	
	/**
	 * 获取已发送短信
	 * @param context 上下文
	 * @since 对于短信的类型Type 接收到的是1 发出去的是2
	 * @return	返回短信对象list集合
	 */
	public static List<SMS> getSentSms(Context context)// 获取已发送短信
	{
		List<SMS> list = new ArrayList<SMS>();
		Uri uri = Uri.parse(SMS_Uri);
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(uri, new String[] { "address", "read", "date", "type", "body" }, null, null, null);
		while (cursor.moveToNext())
		{
			String address = cursor.getString(cursor.getColumnIndex("address"));
			long date = cursor.getLong(cursor.getColumnIndex("date"));
			int type = cursor.getInt(cursor.getColumnIndex("type"));
			String body = cursor.getString(cursor.getColumnIndex("body"));
			int hasread = cursor.getInt(cursor.getColumnIndex("read"));

			if (type == SMS.SENT_SMS)
			{
				list.add(new SMS(address, date, type, body, hasread));
			}
		}
		cursor.close();
		return list;
	}
	
	/**
	 * 获取已接收短信
	 * @param context 上下文
	 * @since 对于短信的类型Type 接收到的是1 发出去的是2
	 * @return	返回接收到的短信list集合
	 */
	public static List<SMS> getReceivedSms(Context context)// 获取已接收短信
	{
		List<SMS> list = new ArrayList<SMS>();
		Uri uri = Uri.parse(SMS_Uri);
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(uri, new String[] { "address", "read", "date", "type", "body" }, null, null, null);
		while (cursor.moveToNext())
		{
			String address = cursor.getString(cursor.getColumnIndex("address"));
			long date = cursor.getLong(cursor.getColumnIndex("date"));
			int type = cursor.getInt(cursor.getColumnIndex("type"));
			String body = cursor.getString(cursor.getColumnIndex("body"));
			int hasread = cursor.getInt(cursor.getColumnIndex("read"));
			if (type == SMS.RECEVIED_SMS)
			{
				list.add(new SMS(address, date, type, body, hasread));
			}
		}
		cursor.close();
		return list;
	}
	
	/**
	 * 获取已读短信
	 * @param context 上下文
	 * @return 已读短信的SMS对象list集合
	 */
	public static List<SMS> getHasReadSMS(Context context)
	{
		List<SMS> list = new ArrayList<SMS>();
		Uri uri = Uri.parse(SMS_Uri);
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(uri, new String[] { "address", "read", "date", "type", "body" }, null, null, null);
		while (cursor.moveToNext())
		{
			String address = cursor.getString(cursor.getColumnIndex("address"));
			long date = cursor.getLong(cursor.getColumnIndex("date"));
			int type = cursor.getInt(cursor.getColumnIndex("type"));
			String body = cursor.getString(cursor.getColumnIndex("body"));
			int hasread = cursor.getInt(cursor.getColumnIndex("read"));
			if (hasread == SMS.Has_Read)
			{
				list.add(new SMS(address, date, type, body, hasread));
			}
		}
		cursor.close();
		return list;
	}
	
	/**
	 * 获取未读短信
	 * @param context 上下文
	 * @return 未读短信的SMS对象list集合
	 */
	public static List<SMS> getHaveNotReadSMS(Context context)
	{
		List<SMS> list = new ArrayList<SMS>();
		Uri uri = Uri.parse(SMS_Uri);
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(uri, new String[] { "address", "read", "date", "type", "body" }, null, null, null);
		while (cursor.moveToNext())
		{
			String address = cursor.getString(cursor.getColumnIndex("address"));
			long date = cursor.getLong(cursor.getColumnIndex("date"));
			int type = cursor.getInt(cursor.getColumnIndex("type"));
			String body = cursor.getString(cursor.getColumnIndex("body"));
			int hasread = cursor.getInt(cursor.getColumnIndex("read"));
			if (hasread == SMS.Have_NOT_Read)
			{
				list.add(new SMS(address, date, type, body, hasread));
			}
		}
		cursor.close();
		return list;
	}
	
	/**
	 * 获取指定号码的短信
	 * @param context 上下文
	 * @param search_address 	需要查询的号码
	 * @return 					指定号码的SMS对象List集合
	 */
	public static List<SMS> getSMSByAddress(Context context,String search_address)
	{
		List<SMS> list = new ArrayList<SMS>();
		Uri uri = Uri.parse(SMS_Uri);
		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(uri, new String[] { "address", "read", "date", "type", "body" }, null, null, null);
		while (cursor.moveToNext())
		{
			String address = cursor.getString(cursor.getColumnIndex("address"));
			long date = cursor.getLong(cursor.getColumnIndex("date"));
			int type = cursor.getInt(cursor.getColumnIndex("type"));
			String body = cursor.getString(cursor.getColumnIndex("body"));
			int hasread = cursor.getInt(cursor.getColumnIndex("read"));
			if (search_address.equals(address))
			{
				list.add(new SMS(address, date, type, body, hasread));
			}
		}
		cursor.close();
		return list;
	}
	/**
	 * 向系统插入短信
	 * @param context 上下文
	 * @param smss	短信对象，空  或者短信对象数组
	 * @return 		插入成功返回true 否则  false
	 */
	public static boolean insertSMS(Context context,SMS... smss)
	{
		try
		{
			if (smss != null)
			{
				ContentResolver resolver = context.getContentResolver();
				Uri uri = Uri.parse(SMS_Uri);
				ContentValues values = null;
				for (SMS sms : smss)
				{
					values = new ContentValues();
					values.put("address", sms.getAddress());
					values.put("date", sms.getDate());
					values.put("type", sms.getType());
					values.put("body", sms.getBody());
					values.put("read", sms.getHasRead());
					resolver.insert(uri, values);
				}
			}
			return true;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 发送短信的方法<b>【适用群发】</b><br>
	 * 需要添加发送短信的权限<br>
	 * <b>android.permission.SEND_SMS</b>
	 * @param contexts 上下文
	 * @param numbers	需要发送的短信的多个号码
	 * @param content	发送的内容
	 * @return			是否发送成功
	 */
	public static boolean sendSMS(Context contexts,String []numbers,String content)
	{
		try
		{
			SmsManager smsManager = SmsManager.getDefault();
			PendingIntent sentIntent = PendingIntent.getBroadcast(contexts, 0, new Intent(), 0);
			if (numbers != null && numbers.length > 0)
			{
				for (String number : numbers)
				{
					if(content.length() >= 70)
			         {
			             //短信字数大于70，自动分条
			             List<String> ms = smsManager.divideMessage(content);
			             for(String str : ms )
			             {
			                 //短信发送
			                 smsManager.sendTextMessage(number, null, str, sentIntent, null);
			             }
			         }
			         else
			         {
			             smsManager.sendTextMessage(number, null, content, sentIntent, null);
			         }
				}
				return true;
			}else 
			{
				throw new Exception("号码不能为空");
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	/**
	 * 发送短信的方法<br>
	 * 需要添加发送短信的权限<br>
	 * <b>android.permission.SEND_SMS</b>
	 * @param context  	上下文
	 * @param number	发送的号码
	 * @param content	发送的内容
	 * @return			是否发送成功
	 */
	public static boolean sendSMS(Context context,String number,String content)
	{
		try
		{
			 SmsManager smsManager = SmsManager.getDefault();
	         PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, new Intent(), 0);
	         if(content.length() >= 70)
	         {
	             //短信字数大于70，自动分条
	             List<String> ms = smsManager.divideMessage(content);
	             for(String str : ms )
	             {
	                 //短信发送
	                 smsManager.sendTextMessage(number, null, str, sentIntent, null);
	             }
	         }
	         else
	         {
	             smsManager.sendTextMessage(number, null, content, sentIntent, null);
	         }
	         return true;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
