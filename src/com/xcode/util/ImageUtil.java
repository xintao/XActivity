package com.xcode.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;

/**
 * 图片工具
 * @author 肖蕾
 */
public class ImageUtil
{
	/**
	 * 根据制定大小获取图片的缩略图
	 * @param context	上下文
	 * @param imgID		图片的ID
	 * @param width		指定的宽
	 * @param height	指定的高
	 * @return			缩略图
	 */
	public static Bitmap getLiteBitmapImage(Context context,int imgID,int width,int height)
	{
		Bitmap bitmap1 = BitmapFactory.decodeResource(context.getResources(),imgID);
		Bitmap bitmap = getLiteBitmapImage(bitmap1, width, height);
		bitmap1 = null;
		return bitmap;
	}
	
	/**
	 * 根据指定大小生成缩略图
	 * @param bitmap	图片原文件
	 * @param width		指定宽度
	 * @param height	指定高度
	 * @return			缩略图
	 */
	public static Bitmap getLiteBitmapImage(Bitmap bitmap,int width,int height)
	{
		Bitmap bitmap1 = ThumbnailUtils.extractThumbnail(bitmap, width, height);
		return bitmap1;
	}
	
	/**
	 * 根据制定大小获取图片的缩略图
	 * @param context	上下文
	 * @param imgID		图片的ID
	 * @param width		指定的宽
	 * @param height	指定的高
	 * @return			缩略图
	 */
	public static Drawable getLiteDrawable(Context context,int imgID,int width,int height)
	{
		Bitmap bitmap1 = BitmapFactory.decodeResource(context.getResources(),imgID);
		Bitmap bitmap = getLiteBitmapImage(bitmap1, width, height);
		Drawable drawable =new BitmapDrawable(bitmap);
		bitmap1 = null;
		bitmap = null;
		return drawable;
	}
	/**
	 * 根据指定大小生成缩略图
	 * @param bitmap	图片原文件
	 * @param width		指定宽度
	 * @param height	指定高度
	 * @return			缩略图
	 */
	public static Drawable getLiteDrawable(Bitmap bitmap,int width,int height)
	{
		Bitmap bitmap1 = getLiteBitmapImage(bitmap, width, height);
		Drawable drawable =new BitmapDrawable(bitmap1);
		bitmap1 = null;
		return drawable;
	}
}
