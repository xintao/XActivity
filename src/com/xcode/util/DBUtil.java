package com.xcode.util;



import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xcode.bean.SQLiteHelper;
import com.xcode.bean.Table;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * 操作SQLite数据库的入口
 * 
 * @author 肖蕾
 */
public class DBUtil
{

	private SQLiteHelper sqLiteHelper = null;

	public DBUtil(Context context, String db_name, String[] Init_sqls)
	{
		sqLiteHelper = new SQLiteHelper(context, db_name, Init_sqls);
		sqLiteHelper.getReadableDatabase().close();
	}

	public DBUtil(Context context, String db_name, Table... initTables)
	{
		String[] sqls = new String[initTables.length];
		/**
		 * 对传过来的Table数组进行遍历
		 */
		for (int i = 0; i < initTables.length; i++)
		{
			/**
			 * 遍历中，把每个Table中的主键 主键属性 以及各字段的map拿出来
			 */
			Table table = initTables[i];
			String keyName = table.keyName;
			String keyType = table.keyType;
			String tabName = table.tabName;
			HashMap<String, String> tabMap = table.tabMap;
			
			
			sqls[i] = "CREATE TABLE if not exists " + tabName + "(" + keyName + " " + keyType + " PRIMARY KEY,";
			
			if (keyName == null || keyName.isEmpty())
			{
				sqls[i] = "CREATE TABLE " + tabName + "(";
			}
			/**
			 * 遍历保存字段以及属性的map 把所有的字段以及字段类型拿出来 以便组成SQL语句
			 */
			for (Map.Entry<String, String> entry : tabMap.entrySet())
			{
				// 字段名
				String Name = entry.getKey();
				// 字段类型
				String Type = entry.getValue();
				sqls[i] += Name + " " + Type + ",";
			}
			sqls[i] = sqls[i].substring(0, sqls[i].length() - 1);
			sqls[i] += ");";
		}
		sqLiteHelper = new SQLiteHelper(context, db_name, sqls);
		sqLiteHelper.getReadableDatabase().close();
	}

	public DBUtil(Context context, String db_name)
	{
		sqLiteHelper = new SQLiteHelper(context, db_name, null);
		sqLiteHelper.getReadableDatabase().close();
	}

	/**
	 * 获取指向链接数据库的对象,可能使用者需要进行一些我没有考虑到的操作， 或者需要查询 操作然后手动释放sqlitedatabase.close()
	 */
	public SQLiteDatabase getSqliteDatabase()
	{
		if (sqLiteHelper == null)
		{
			return null;
		}
		return sqLiteHelper.getWritableDatabase();
	}

	/**
	 * 往指定表中插入数据
	 * 
	 * @param tableName 	表名
	 * @param values 		包含要插入的数据values对象
	 * @return 				返回受影响的行数
	 */
	public int insertInto(String tableName, ContentValues... values)
	{
		SQLiteDatabase sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		long num = 0;
		for (ContentValues value : values)
		{
			num = sqLiteDatabase.insert(tableName, null, value);
		}
		sqLiteDatabase.close();
		return (int) num;
	}

	/**
	 * 删除指定表中的数据
	 * 
	 * @param tableName 	表名
	 * @param column_value 	一个map，key_value 分别保存了字段名与其值
	 * @return 				返回删除的行数
	 */
	public int delete(String tableName, Map<String, String> column_value)
	{
		SQLiteDatabase sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		String whereClause = "";
		String whereArgs[] = new String[column_value.entrySet().size()];
		int num = 0;
		for (Map.Entry<String, String> entry : column_value.entrySet())
		{
			String column = entry.getKey();
			whereClause += column + "=? and ";
			String value = entry.getValue();
			whereArgs[num] = value;
			num++;
		}
		whereClause = whereClause.substring(0, whereClause.lastIndexOf(" and "));
		num = sqLiteDatabase.delete(tableName, whereClause, whereArgs);
		sqLiteDatabase.close();
		return num;
	}

	/**
	 * 更新指定表中的数据
	 * 
	 * @param tableName 	表名
	 * @param setNewValue 	需要设置的字段名 以及相对应的值
	 * @param where 		作为条件的字段名 以及相对应的值
	 * @return 				返回更新的行数
	 */
	public int update(String tableName, Map<String, String> setNewValue,Map<String, String> where)
	{
		SQLiteDatabase sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		for (Map.Entry<String, String> newValueEntry : setNewValue.entrySet())
		{
			String colum = newValueEntry.getKey();
			String value = newValueEntry.getValue();
			values.put(colum, value);
		}
		String whereClause = "";
		String whereArgs[] = new String[where.entrySet().size()];
		int num = 0;
		for (Map.Entry<String, String> entry : where.entrySet())
		{
			String column = entry.getKey();
			whereClause += column + "=? and ";
			String value = entry.getValue();
			whereArgs[num] = value;
			num++;
		}
		whereClause = whereClause.substring(0, whereClause.lastIndexOf(" and "));

		num = sqLiteDatabase.update(tableName, values, whereClause, whereArgs);
		sqLiteDatabase.close();
		return num;
	}

	/**
	 * 根据指定条件查询全部数据
	 * 
	 * @param tableName 要查询的表
	 * @param where_column_value
	 *            where条件 为什么需要 sqLiteDatabase
	 *            因为需要使用者手动sqLiteDatabase.close()，避免出现android
	 *            .database.sqlite.DatabaseObjectNotClosedException
	 * @param sqLiteDatabase 链接数据库的对象
	 * @return 查询到的结果集
	 */
	public Cursor selectAllBy(String tableName,Map<String, String> where_column_value,SQLiteDatabase sqLiteDatabase)
	{
		String whereClause = "";
		String whereArgs[] = new String[where_column_value.entrySet().size()];
		int num = 0;
		for (Map.Entry<String, String> entry : where_column_value.entrySet())
		{
			String column = entry.getKey();
			whereClause += column + "=? and ";
			String value = entry.getValue();
			whereArgs[num] = value;
			num++;
		}
		whereClause = whereClause.substring(0, whereClause.lastIndexOf(" and "));
		return sqLiteDatabase.query(tableName, null, whereClause, whereArgs,null, null, null);
	}

	/**
	 * 查询指定的字段 colums 通过条件 where_column_value
	 * 
	 * @param tableName
	 *            查询的表
	 * @param columns
	 *            指定的字段
	 * @param where_column_value
	 *            查询的条件 为什么需要 sqLiteDatabase
	 *            因为需要使用者手动sqLiteDatabase.close()，避免出现android
	 *            .database.sqlite.DatabaseObjectNotClosedException
	 * @param sqLiteDatabase
	 * @return 查询到的结果集
	 */
	public Cursor selectColumnBy(String tableName, String[] columns,Map<String, String> where_column_value,SQLiteDatabase sqLiteDatabase)
	{
		String whereClause = "";
		String whereArgs[] = new String[where_column_value.entrySet().size()];
		int num = 0;
		for (Map.Entry<String, String> entry : where_column_value.entrySet())
		{
			String column = entry.getKey();
			whereClause += column + "=? and ";
			String value = entry.getValue();
			whereArgs[num] = value;
			num++;
		}
		whereClause = whereClause.substring(0, whereClause.lastIndexOf(" and "));
		return sqLiteDatabase.query(tableName, columns, whereClause, whereArgs,null, null, null);
	}

	/**
	 * 查询指定表中所有的数据
	 * 
	 * @param tableName
	 *            指定的表 为什么需要 sqLiteDatabase
	 *            因为需要使用者手动sqLiteDatabase.close()，避免出现android
	 *            .database.sqlite.DatabaseObjectNotClosedException
	 * @param sqLiteDatabase
	 * @return 查询到的结果集
	 */
	public Cursor selectAllByTableName(String tableName,
			SQLiteDatabase sqLiteDatabase)
	{
		Cursor cursor = sqLiteDatabase.query(tableName, null, null, null, null,
				null, null);
		return cursor;
	}

	/**
	 * 执行自己的SQL语句
	 * 
	 * @param sql
	 *            SQL语句
	 */
	public void executeSql(String sql)
	{
		SQLiteDatabase sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		sqLiteDatabase.execSQL(sql);
		sqLiteDatabase.close();
	}

	/**
	 * 执行预编译的SQL语句
	 * 
	 * @param sql
	 *            预编译的SQL语句
	 * @param params
	 *            预编译后填入的参数
	 */
	public void executeSql(String sql, String params[])
	{
		SQLiteDatabase sqLiteDatabase = sqLiteHelper.getWritableDatabase();
		sqLiteDatabase.execSQL(sql, params);
		sqLiteDatabase.close();
	}

	/**
	 * 专门释放链接数据库资源的方法，可以释放的类型是：Cursor，SQLiteDatabase
	 */
	public void close(Object... Cursor_Or_SQLiteDatabase)
	{
		for (Object object : Cursor_Or_SQLiteDatabase)
		{
			if (object != null)
			{
				if (object instanceof Cursor)
				{
					Cursor cursor = (Cursor) object;
					cursor.close();
				} else if (object instanceof SQLiteDatabase)
				{
					SQLiteDatabase sqLiteDatabase = (SQLiteDatabase) object;
					sqLiteDatabase.close();
				}

			}
		}
	}
	
	/**
	 * @param cursor				查询到的   ResultSet结果集
	 * @param javaBeanClass		需要转换的 JavaBean
	 * @return					一个List集合，里面包含从ResultSet结果集转换到JavaBean的对象
	 */
	public static List<Object> DB2JavaBean(Cursor cusor, Class<?> javaBeanClass)
	{
		List<Object> list = null;
		
		Field []fields = javaBeanClass.getDeclaredFields();
		
		try
		{
			list = new ArrayList<Object>();
			Method[] methods = javaBeanClass.getDeclaredMethods(); // 获取这个类里面所有的方法
			while (cusor.moveToNext())
			{
				Object object = javaBeanClass.newInstance();

				for (Method method : methods) // 遍历所有的方法
				{
					if (method.getName().startsWith("set")) // 获取所有Set的Method
					{
						// System.out.println(method.getName());
						Class<?> class1[] = method.getParameterTypes();
						Object objs[] = new Object[class1.length];
						try
						{	//如果这个方法不能执行   就跳过这个方法不执行
							//（可能数据库里面没有这个字段）
							for (int i = 0; i < class1.length; i++)
							{
								String clomm = method.getName().substring(method.getName().indexOf("set")+ "set".length());
								for (Field field : fields)
								{
									String fieName = field.getName();
									if (fieName.equalsIgnoreCase(clomm.toUpperCase()))
									{
										clomm = field.getName();
									}
								}
								int cursorIndex = cusor.getColumnIndex(clomm);
								if (cursorIndex == -1)
								{
									continue;
								}
								
								if (class1[i].getName().equals(int.class.toString()))// 如果是基本类型的话 -int
								{
									objs[i] = Integer.valueOf(cusor.getInt(cursorIndex));
								} else if (class1[i].getName().equals(byte.class.toString())) // 如果是基本类型的话 -byte
								{
									objs[i] = Byte.valueOf((byte)cusor.getInt(cursorIndex));
								} else if (class1[i].getName().equals(boolean.class.toString())) // 如果是基本类型的话 -boolean
								{
									objs[i] = Boolean.valueOf(cusor.getString(cursorIndex));
								} else if (class1[i].getName().equals(char.class.toString())) // 如果是基本类型的话 -char
								{
									objs[i] =Character.valueOf((char) cusor.getInt(cursorIndex));
								} else if (class1[i].getName().equals(float.class.toString())) // 如果是基本类型的话 -Float
								{
									objs[i] = Float.valueOf(cusor.getFloat(cursorIndex));
								} else if (class1[i].getName().equals(double.class.toString())) // 如果是基本类型的话 -double
								{
									objs[i] = Double.valueOf(cusor.getDouble(cursorIndex));
								} else if (class1[i].getName().equals(long.class.toString())) // 如果是基本类型的话 -long
								{
									objs[i] = Long.valueOf(cusor.getLong(cursorIndex));
								} else
								{
									objs[i] = cusor.getString(cursorIndex);
								}

							}
							method.invoke(object, objs);
						} catch (Exception e)
						{
							continue;
						}
					}
				}
				list.add(object);
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
	
}
