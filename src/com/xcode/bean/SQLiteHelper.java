package com.xcode.bean;



import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper
{

	private String[] initsql = null;

	public SQLiteHelper(Context context, String db_name, String[] Init_sqls)
	{
		super(context, db_name, null, 1);
		this.initsql = Init_sqls;
	}
	/**
	 * 在第一次运行的时候才会执行回调这个方法 一般使用与数据库的初始化， 表的建立
	 */
	@Override
	public void onCreate(SQLiteDatabase db)
	{
		if (initsql != null && initsql.length > 0)
		{
			for (String sql : initsql)
			{
				db.execSQL(sql);
			}
		}
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{

	}

}
